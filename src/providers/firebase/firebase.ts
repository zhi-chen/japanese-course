import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
/*
  Generated class for the FirebaseProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class FirebaseProvider {

  // constructor(public http: Http) {
  //   console.log('Hello FirebaseProvider Provider');
  // }

  constructor(public db: AngularFireDatabase, public af: AngularFireModule) { }

  getcourseList() {
    var query = this.db.database.ref('courses').once('value').then(res => res);
    return query;
  }

  getCourseList() {
    // console.log(this.db.list('/courses/'));
    return this.db.list('/courses/');
  }

  getLessonListByCourseId(courseId:number){
     var query = this.db.database.ref('courses').equalTo(1,'id').orderByChild('lessons');
     return this.db.list(query);
  }

  getWholeDb() {

    let result = this.db.object('/courses/').subscribe(result => {
      console.log('result');
      console.log(result);
    });
    return this.db.list('/');

    //FirebaseDatabase.getInstance().getReference("node_1/node_2");
  }

  //  getShoppingItems() {
  //    return this.afd.list('/shoppingItems/');
  //  }

  //  addItem(name) {
  //    this.afd.list('/shoppingItems/').push(name);
  //  }

  //  removeItem(id) {
  //    this.afd.list('/shoppingItems/').remove(id);
  //  }

}
