import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the CourseProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class CourseProvider {

  url;

  constructor(public http: Http) {
    console.log('Hello CourseProvider Provider');
    this.url = 'https://japanesecoursewithpiotr.firebaseio.com/courses.json';    
  }

  getCourse() {
    // return this.http.get(this.url)
    //   .map(res => res.json());
    return this.http.get(this.url)
      .map(res => res.json());
  }

}
