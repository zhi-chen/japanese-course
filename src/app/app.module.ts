import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { CoursePage } from '../pages/courses/courses';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { Lessons } from '../pages/lessons/lessons';
import { LessonPage } from '../pages/lesson/lesson';
import { LoginPage } from '../pages/login/login';
import { TestsPage } from '../pages/tests/tests';


import { HttpModule } from '@angular/http';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireModule } from 'angularfire2';
import { FirebaseProvider } from '../providers/firebase/firebase';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { CourseProvider } from '../providers/course/course';

const firebaseConfig = {
    apiKey: "AIzaSyDNVca_1bhyHL_-nvEs61eoTXchlSLnPPI",
    authDomain: "japanesecoursewithpiotr.firebaseapp.com",
    databaseURL: "https://japanesecoursewithpiotr.firebaseio.com",
    projectId: "japanesecoursewithpiotr",
    storageBucket: "japanesecoursewithpiotr.appspot.com",
    messagingSenderId: "400567341682"
  };

@NgModule({
  declarations: [
    MyApp,
    CoursePage,
    ContactPage,
    HomePage,
    TabsPage,
    Lessons,
    LessonPage,
    LoginPage,
    TestsPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    AngularFireDatabaseModule,
    AngularFireModule,
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    CoursePage,
    ContactPage,
    HomePage,
    TabsPage,
    Lessons,
    LessonPage,
    LoginPage,
    TestsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    FirebaseProvider,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    FirebaseProvider,
    CourseProvider
  ]
})
export class AppModule {}
