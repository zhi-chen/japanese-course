import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ViewChild } from '@angular/core';

import { Slides } from 'ionic-angular';

/**
 * Generated class for the TestsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tests',
  templateUrl: 'tests.html',
})
export class TestsPage {
  @ViewChild(Slides) slides: Slides;
  question0;
  question1;
  question2;
  isCheckClicked = false;
  score = 0;
  
  
  slidesContent = [
    {
      title: "What is sushi in Japanese?",
      optionA: "寿司",
      optionB: "服",
      rightAnswer: "a",
      image: "assets/img/003-sushi-1.png",
    },
    {
      title: "What is cat in Japanese?",
      optionA: "猫",
      optionB: "犬",
      rightAnswer: "a",
      image: "/assets/img/004-maneki-neko.png",
    },
    {
      title: "What is lotus in Japanese?",
      optionA: "桜",
      optionB: "蓮",
      rightAnswer: "b",
      image: "assets/img/005-lotus.png",
    }
  ];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TestsPage');
  }

  public goToNextSlide() {
    this.slides.slideNext(500);
  }

  public checkOnClick() {
    this.score = 0;
    this.getScore();
    this.isCheckClicked = true;
    return this.isCheckClicked;
  }

  

  public getScore() {
    
    if(this.question0 =="a"){
      this.score++;
    }
    if(this.question1 =="a"){
      this.score++;
    }
    if(this.question2 =="b"){
      this.score++;
    }
    return this.score;
  }

  public isScoreGreaterThanTwo() {
    if(this.score >= 2){
      return true;
    } else return false;
  }
}
