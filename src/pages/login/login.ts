import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';


/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  username;
  password;
  validateUser = true;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  public login() {
    console.log(this.username);
    console.log(this.password);
    if (this.isValidateUser()){
      console.log("Validate");
      this.validateUser = true;
      this.navCtrl.push(TabsPage);
    } else {
      this.validateUser = false;
    }
  }

  public isValidateUser() {
    let isValidate = false;
    if(this.username=="chenzhi.finland@gmail.com" && this.password=="12345678"){
      isValidate = true;
    }
    return isValidate;
  }

}
