import { Component } from '@angular/core';
import { LessonPage } from '../lesson/lesson'
import { NavController } from 'ionic-angular';
import { FirebaseProvider } from './../../providers/firebase/firebase';
import { FirebaseListObservable } from 'angularfire2/database';
import { CourseProvider } from './../../providers/course/course';


@Component({
  templateUrl: 'lessons.html'
})
export class Lessons {
  courses;
  items = [];
  beginnerLessons;

  constructor(public navCtrl: NavController, public courseProvider: CourseProvider) {
    
  }

  ionViewWillEnter() {
    this.courseProvider.getCourse()
    .subscribe((courses => {
      this.courses = courses;
      this.beginnerLessons = courses[0].lessons;
      console.log('this.beginnerLessons',this.beginnerLessons);
      this.beginnerLessons.forEach(element => {
        this.items.push(element.name);
      });
    }));
  }

  public itemSelected(item: string) {
    console.log("Selected Item", item);
    this.navCtrl.push(LessonPage);
  }
}
