import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Lessons } from '../lessons/lessons';
import { CourseProvider } from './../../providers/course/course';

@Component({
  selector: 'page-course',
  templateUrl: 'courses.html'
})
export class CoursePage {
  courses;

  constructor(public navCtrl: NavController, public courseProvider: CourseProvider) {    
  }

  ionViewWillEnter() {
    this.courseProvider.getCourse()
      .subscribe((courses => {
        this.courses = courses;
        console.log('this.courseList',this.courses);
      }));
    
    // console.log('this.courseList',this.courses);
  }

  public navigate(id: number) {
    this.navCtrl.push(Lessons);
  }

}
